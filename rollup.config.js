export default {
	entry: 'dist/index.js',
	dest: 'dist/bundles/angular-auth-states.umd.js',
	sourceMap: false,
	format: 'umd',
	moduleName: 'ng.angular-auth-states',
	globals: {
		'@angular/core': 'ng.core',
		'@angular/common': 'ng.common',
		'@angular/common/http': 'ng.common.http',
		'rxjs/Observable': 'Rx',
		'rxjs/Subject': 'Rx',
		'rxjs/add/operator/map': 'Rx.Observable.prototype',
		'rxjs/add/operator/catch': 'Rx.Observable.prototype',
		'rxjs/add/operator/take': 'Rx.Observable.prototype',
		'rxjs/add/operator/mergeMap': 'Rx.Observable.prototype',
		'rxjs/add/operator/mergeAll': 'Rx.Observable.prototype',
		'rxjs/add/observable/throw': 'Rx.Observable',
		'rxjs/add/observable/of': 'Rx.Observable',
		'rxjs/add/observable/range': 'Rx.Observable',
		'rxjs/add/observable/empty': 'Rx.Observable'
	}
}
