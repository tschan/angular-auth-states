import { TestBed, inject } from '@angular/core/testing';

import { AuthInterceptorService } from './auth-interceptor.service';
import { AuthService } from "./auth.service";
import { AuthStateMachineService } from "./auth-state-machine.service";
import { AuthEventBusService } from "./auth-event-bus.service";
import { AuthBufferService } from "./auth-buffer.service";

describe('AuthInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthInterceptorService,
        AuthService,
        AuthStateMachineService,
        AuthEventBusService,
        AuthBufferService
      ]
    });
  });

  it('should be created', inject([AuthInterceptorService], (service: AuthInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
