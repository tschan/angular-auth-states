import { Injectable } from '@angular/core';

import { AUTH_INPUTS, AUTH_STATES } from "./constants";
import { AuthStateMachineService } from "./auth-state-machine.service";
import { AuthEventBusService, AuthEvent } from "./auth-event-bus.service";
import { RequestUpdater } from "./types";


@Injectable()
export class AuthService {

  private configUpdaterFunction: RequestUpdater = req => req;

  constructor(private stateMachine: AuthStateMachineService, private eventBusService: AuthEventBusService) {}

  public getConfigUpdaterFunction() {
    return this.configUpdaterFunction;
  }

  public setConfigUpdaterFunction(updater: RequestUpdater) {
    this.configUpdaterFunction = updater;
  }

  public getEventBus() {
    return this.eventBusService.eventBus;
  }

  public getCurrentState() {
    return this.stateMachine.getCurrentState();
  }

  public login(updater?: RequestUpdater, data?: any) {
    return this.stateMachine.doTransition(AUTH_INPUTS.LOGIN, data, {updater: updater});
  }

  public logout(reason?: any, data?: any) {
    return this.stateMachine.doTransition(AUTH_INPUTS.LOGOUT, data, {reason: reason});
  }

}
