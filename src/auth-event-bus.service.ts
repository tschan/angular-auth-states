import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/index';

import { AUTH_EVENTS } from "./constants";


export class AuthEvent {

  constructor(public eventType: AUTH_EVENTS, public data?: any) {}

}

@Injectable()
export class AuthEventBusService {

  readonly eventBus = new Subject<AuthEvent>();

  public sendAuthEvent(eventType: AUTH_EVENTS, data?: any) {
    this.eventBus.next(new AuthEvent(eventType, data));
  }

}
