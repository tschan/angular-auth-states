import { TestBed, inject } from '@angular/core/testing';

import { AuthEventBusService } from './auth-event-bus.service';

describe('AuthEventBusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthEventBusService]
    });
  });

  it('should be created', inject([AuthEventBusService], (service: AuthEventBusService) => {
    expect(service).toBeTruthy();
  }));
});
