import { TestBed, inject } from '@angular/core/testing';

import { AuthStateMachineService } from './auth-state-machine.service';
import { AuthEventBusService } from "./auth-event-bus.service";
import { AuthBufferService } from "./auth-buffer.service";

describe('AuthStateMachineService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthStateMachineService,
        AuthEventBusService,
        AuthBufferService
      ]
    });
  });

  it('should be created', inject([AuthStateMachineService], (service: AuthStateMachineService) => {
    expect(service).toBeTruthy();
  }));
});
