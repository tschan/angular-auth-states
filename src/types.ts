import { HttpRequest } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';


export type RequestUpdater = (req: HttpRequest<any>) => HttpRequest<any>;
export type TransitionOptions = {subject?: Subject<RequestUpdater>, updater?: RequestUpdater, reason?: any}
