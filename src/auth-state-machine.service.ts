import { Injectable } from '@angular/core';
import { Observable, EMPTY, throwError, range, of } from 'rxjs/index';
import { take, map, mergeMap, mergeAll, catchError } from 'rxjs/operators';

import { AUTH_STATES, AUTH_INPUTS, AUTH_EVENTS } from "./constants";
import { AuthEventBusService } from "./auth-event-bus.service";
import { AuthBufferService } from "./auth-buffer.service";
import { TransitionOptions } from "./types";


class Transition<S, O> {

  constructor(private nextState?: S, private action?: (data: any, options?: O) => any, private preAction?: (data: any) => any) {}

  getNextState() {
    return this.nextState;
  }

  executePreAction(data: any) {
    let result = of(true);
    if (this.preAction != undefined) {
      try {
        let res = this.preAction(data);
        if (res instanceof Observable) {
          result = res;
        }
      } catch (err) {
        result = throwError(err);
      }
    }
    return result;
  }

  executeAction(data: any, options?: O) {
    if (this.action != undefined) {
      try {
        this.action(data, options);
      } catch (err) {
        // do nothing
      }
    }
  }

}

class StateMachine<S,I,O> {

  public currentState: S;
  private transitions: Map<S, Map<I, Transition<S,O>>>;
  private transitionInProgress?: Observable<S>;

  constructor(private startState: S) {
    this.transitions = new Map<S, Map<I, Transition<S,O>>>();
    this.currentState = startState;
  }

  public addTransition(fromState: S, input: I, transition: Transition<S,O>) {
    if (!this.transitions.has(fromState)) {
      this.transitions.set(fromState, new Map<I, Transition<S,O>>());
    }
    const inputMap = this.transitions.get(fromState);
    if (inputMap != undefined) {
      inputMap.set(input, transition);
    }
  }

  doTransition(input: I, data?: any, options?: O) {
    if (this.transitionInProgress != undefined) {
      return this.transitionInProgress;
    }
    const inputMap = this.transitions.get(this.currentState);
    if (inputMap != undefined) {
      const transition = inputMap.get(input);
      if (transition != undefined) {
        let observable = transition.executePreAction(data)
          .pipe(
            take(1),
            mergeMap(val => {
              if (val) {
                return range(1,2);
              } else {
                return throwError('transition was aborted');
              }
            }),
            map(val => {
              if (val === 1) {
                // indicate to the subscriber that the preAction was successfully executed,
                // but the actual action has not yet been executed
                return of(this.currentState);
              }
              const nextState = transition.getNextState();
              if (nextState != undefined) {
                this.currentState = nextState;
              }
              transition.executeAction(data, options);
              this.transitionInProgress = undefined;
              return EMPTY;
            }),
            mergeAll(),
            catchError(err => {
              this.transitionInProgress = undefined;
              return throwError(err);
            })
          );
        this.transitionInProgress = observable;
        return this.transitionInProgress;
      }
    }
    return EMPTY;
  }

}

@Injectable()
export class AuthStateMachineService {

  private stateMachine: StateMachine<AUTH_STATES, AUTH_INPUTS, TransitionOptions>;

  constructor(private eventBusService: AuthEventBusService, private bufferService: AuthBufferService) {
    this.stateMachine = new StateMachine<AUTH_STATES, AUTH_INPUTS, TransitionOptions>(AUTH_STATES.UNINITIALIZED);

    const doLogout = new Transition<AUTH_STATES, TransitionOptions>(
      AUTH_STATES.LOGGED_OUT,
      data => this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGGED_OUT, data),
      data => this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGGED_OUT, data)
    );

    const doLogin = new Transition<AUTH_STATES, TransitionOptions>(
      AUTH_STATES.LOGGED_IN,
      data => this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGGED_IN, data),
      data => this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGGED_IN, data)
    );

    const rejectUnauthorizedRequest = new Transition<AUTH_STATES, TransitionOptions>(
      undefined,
      (data, options) => {
        if (options != undefined && options.subject != undefined) {
          options.subject.error(undefined);
        }
      }
    );

    this.stateMachine.addTransition(AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.LOGOUT, doLogout);
    this.stateMachine.addTransition(AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.LOGIN, doLogin);
    this.stateMachine.addTransition(AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.UNAUTHORIZED, rejectUnauthorizedRequest);

    this.stateMachine.addTransition(AUTH_STATES.LOGGED_OUT, AUTH_INPUTS.LOGIN, doLogin);
    this.stateMachine.addTransition(AUTH_STATES.LOGGED_OUT, AUTH_INPUTS.UNAUTHORIZED, rejectUnauthorizedRequest);

    this.stateMachine.addTransition(AUTH_STATES.LOGGED_IN, AUTH_INPUTS.LOGOUT, doLogout);
    this.stateMachine.addTransition(AUTH_STATES.LOGGED_IN, AUTH_INPUTS.UNAUTHORIZED, new Transition<AUTH_STATES, TransitionOptions>(
      AUTH_STATES.LOGIN_REQUIRED,
      (data, options) => {
        if (options != undefined && options.subject != undefined) {
          this.bufferService.addRetrySubject(options.subject);
        }
        this.eventBusService.sendAuthEvent(AUTH_EVENTS.UNAUTHORIZED, data)
      },
      data => this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_UNAUTHORIZED, data)
    ));

    this.stateMachine.addTransition(AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.LOGIN, new Transition<AUTH_STATES, TransitionOptions>(
      AUTH_STATES.LOGGED_IN,
      (data, options) => {
        this.bufferService.triggerRetry(options && options.updater);
        this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGIN_CONFIRMED, data)
      },
      data => this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGIN_CONFIRMED, data)
    ));

    this.stateMachine.addTransition(AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.LOGOUT, new Transition<AUTH_STATES, TransitionOptions>(
      AUTH_STATES.LOGGED_OUT,
      (data, options) => {
        this.bufferService.rejectAll(options && options.reason);
        this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGIN_REJECTED, data)
        this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGGED_OUT, data)
      },
      data => {
        this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGIN_REJECTED, data)
        this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGGED_OUT, data)
      }
    ));

    this.stateMachine.addTransition(AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.UNAUTHORIZED, new Transition<AUTH_STATES, TransitionOptions>(
      undefined,
      (data, options) => {
        if (options != undefined && options.subject != undefined) {
          this.bufferService.addRetrySubject(options.subject);
        }
      },
    ));

  }

  doTransition(input: AUTH_INPUTS, data?: any, options?: TransitionOptions) {
    return this.stateMachine.doTransition(input, data, options);
  }

  getCurrentState() {
    return this.stateMachine.currentState;
  }

}
