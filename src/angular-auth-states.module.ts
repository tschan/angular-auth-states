import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthInterceptorService } from "./auth-interceptor.service";
import { AuthService } from "./auth.service";
import { AuthBufferService } from "./auth-buffer.service";
import { AuthStateMachineService } from "./auth-state-machine.service";
import { AuthEventBusService } from "./auth-event-bus.service";


@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true,
    },
    AuthService,
    AuthBufferService,
    AuthStateMachineService,
    AuthEventBusService
]
})
export class AngularAuthStatesModule { }
