import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/index';

import { RequestUpdater } from "./types";


@Injectable()
export class AuthBufferService {

  private retrySubjects : Array<Subject<RequestUpdater>> = [];

  public addRetrySubject(subject: Subject<RequestUpdater>) {
    this.retrySubjects.push(subject);
  }

  triggerRetry(updater?: RequestUpdater) {
    this.retrySubjects.forEach(subject => {
      subject.next(updater);
      subject.complete();
    });
    this.retrySubjects = [];
  }

  rejectAll(reason?: any) {
    this.retrySubjects.forEach(subject => {
      subject.error(reason);
    });
    this.retrySubjects = [];
  }

}
