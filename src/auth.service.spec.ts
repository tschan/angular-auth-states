import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AuthEventBusService } from "./auth-event-bus.service";
import { AuthStateMachineService } from "./auth-state-machine.service";
import { AuthBufferService } from "./auth-buffer.service";

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        AuthEventBusService,
        AuthStateMachineService,
        AuthBufferService
      ]
    });
  });

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));
});
