import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs/index';
import { mergeMap, catchError, take } from 'rxjs/operators';

import { IGNORE_AUTH_HEADER_NAME, AUTH_INPUTS } from "./constants";
import { AuthService } from "./auth.service";
import { AuthStateMachineService } from "./auth-state-machine.service";
import { RequestUpdater } from "./types";


@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService, private stateMachine: AuthStateMachineService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let requestActual = req;
    if (req.headers.has(IGNORE_AUTH_HEADER_NAME)) {
      if (req.headers.get(IGNORE_AUTH_HEADER_NAME)) {
        console.warn('Your request has a value for the IGNORE_AUTH_HEADER_NAME. It will be deleted: ', req)
      }
      requestActual = req.clone({headers: req.headers.delete(IGNORE_AUTH_HEADER_NAME)})
      return next.handle(requestActual)
    } else {
      requestActual = this.updateHttpRequest(req);
      return next
        .handle(requestActual)
        .pipe(catchError(error => {
          if (error instanceof HttpErrorResponse && error.status === 401) {
            const subject = new Subject<RequestUpdater>();
            this.stateMachine.doTransition(AUTH_INPUTS.UNAUTHORIZED, error, {subject: subject}).subscribe();
            return subject
              .pipe(
                take(1),
                catchError(err => throwError(err || error)),
                mergeMap((updater?: RequestUpdater) => {
                  let updatedRequest = requestActual;
                  if (updater != undefined) {
                    updatedRequest = updater(updatedRequest);
                  } else {
                    updatedRequest = this.updateHttpRequest(updatedRequest);
                  }
                  return next.handle(updatedRequest);
                })
              );
          }
          return throwError(error);
        }));
    }
  }

  private updateHttpRequest(request: HttpRequest<any>) {
    return this.authService.getConfigUpdaterFunction()(request);
  }

}
