import { TestBed, inject } from '@angular/core/testing';

import { AuthBufferService } from './auth-buffer.service';

describe('AuthBufferService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthBufferService]
    });
  });

  it('should be created', inject([AuthBufferService], (service: AuthBufferService) => {
    expect(service).toBeTruthy();
  }));
});
