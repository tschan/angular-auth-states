import { Observable } from 'rxjs/index';
import { AUTH_STATES, AUTH_INPUTS } from "./constants";
import { AuthEventBusService } from "./auth-event-bus.service";
import { AuthBufferService } from "./auth-buffer.service";
import { TransitionOptions } from "./types";
export declare class AuthStateMachineService {
    private eventBusService;
    private bufferService;
    private stateMachine;
    constructor(eventBusService: AuthEventBusService, bufferService: AuthBufferService);
    doTransition(input: AUTH_INPUTS, data?: any, options?: TransitionOptions): Observable<AUTH_STATES>;
    getCurrentState(): AUTH_STATES;
}
