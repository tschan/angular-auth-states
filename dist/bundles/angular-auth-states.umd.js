(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/common/http'), require('rxjs/index'), require('rxjs/operators')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common', '@angular/common/http', 'rxjs/index', 'rxjs/operators'], factory) :
	(factory((global.ng = global.ng || {}, global.ng['angular-auth-states'] = global.ng['angular-auth-states'] || {}),global.ng.core,global.ng.common,global.ng.common.http,global.rxjs_index,global.rxjs_operators));
}(this, (function (exports,_angular_core,_angular_common,_angular_common_http,rxjs_index,rxjs_operators) { 'use strict';

var IGNORE_AUTH_HEADER_NAME = 'ng-AuthService-ignoreAuthModule';

(function (AUTH_STATES) {
    AUTH_STATES[AUTH_STATES["UNINITIALIZED"] = 0] = "UNINITIALIZED";
    AUTH_STATES[AUTH_STATES["LOGGED_OUT"] = 1] = "LOGGED_OUT";
    AUTH_STATES[AUTH_STATES["LOGGED_IN"] = 2] = "LOGGED_IN";
    AUTH_STATES[AUTH_STATES["LOGIN_REQUIRED"] = 3] = "LOGIN_REQUIRED";
})(exports.AUTH_STATES || (exports.AUTH_STATES = {}));
var AUTH_INPUTS;
(function (AUTH_INPUTS) {
    AUTH_INPUTS[AUTH_INPUTS["LOGOUT"] = 0] = "LOGOUT";
    AUTH_INPUTS[AUTH_INPUTS["LOGIN"] = 1] = "LOGIN";
    AUTH_INPUTS[AUTH_INPUTS["UNAUTHORIZED"] = 2] = "UNAUTHORIZED";
})(AUTH_INPUTS || (AUTH_INPUTS = {}));

(function (AUTH_EVENTS) {
    AUTH_EVENTS[AUTH_EVENTS["PRE_LOGGED_OUT"] = 0] = "PRE_LOGGED_OUT";
    AUTH_EVENTS[AUTH_EVENTS["LOGGED_OUT"] = 1] = "LOGGED_OUT";
    AUTH_EVENTS[AUTH_EVENTS["PRE_LOGGED_IN"] = 2] = "PRE_LOGGED_IN";
    AUTH_EVENTS[AUTH_EVENTS["LOGGED_IN"] = 3] = "LOGGED_IN";
    AUTH_EVENTS[AUTH_EVENTS["PRE_UNAUTHORIZED"] = 4] = "PRE_UNAUTHORIZED";
    AUTH_EVENTS[AUTH_EVENTS["UNAUTHORIZED"] = 5] = "UNAUTHORIZED";
    AUTH_EVENTS[AUTH_EVENTS["PRE_LOGIN_CONFIRMED"] = 6] = "PRE_LOGIN_CONFIRMED";
    AUTH_EVENTS[AUTH_EVENTS["LOGIN_CONFIRMED"] = 7] = "LOGIN_CONFIRMED";
    AUTH_EVENTS[AUTH_EVENTS["PRE_LOGIN_REJECTED"] = 8] = "PRE_LOGIN_REJECTED";
    AUTH_EVENTS[AUTH_EVENTS["LOGIN_REJECTED"] = 9] = "LOGIN_REJECTED";
})(exports.AUTH_EVENTS || (exports.AUTH_EVENTS = {}));

var __decorate$4 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AuthEvent = /** @class */ (function () {
    function AuthEvent(eventType, data) {
        this.eventType = eventType;
        this.data = data;
    }
    return AuthEvent;
}());
var AuthEventBusService = /** @class */ (function () {
    function AuthEventBusService() {
        this.eventBus = new rxjs_index.Subject();
    }
    AuthEventBusService.prototype.sendAuthEvent = function (eventType, data) {
        this.eventBus.next(new AuthEvent(eventType, data));
    };
    AuthEventBusService = __decorate$4([
        _angular_core.Injectable()
    ], AuthEventBusService);
    return AuthEventBusService;
}());

var __decorate$5 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AuthBufferService = /** @class */ (function () {
    function AuthBufferService() {
        this.retrySubjects = [];
    }
    AuthBufferService.prototype.addRetrySubject = function (subject) {
        this.retrySubjects.push(subject);
    };
    AuthBufferService.prototype.triggerRetry = function (updater) {
        this.retrySubjects.forEach(function (subject) {
            subject.next(updater);
            subject.complete();
        });
        this.retrySubjects = [];
    };
    AuthBufferService.prototype.rejectAll = function (reason) {
        this.retrySubjects.forEach(function (subject) {
            subject.error(reason);
        });
        this.retrySubjects = [];
    };
    AuthBufferService = __decorate$5([
        _angular_core.Injectable()
    ], AuthBufferService);
    return AuthBufferService;
}());

var __decorate$3 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata$2 = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Transition = /** @class */ (function () {
    function Transition(nextState, action, preAction) {
        this.nextState = nextState;
        this.action = action;
        this.preAction = preAction;
    }
    Transition.prototype.getNextState = function () {
        return this.nextState;
    };
    Transition.prototype.executePreAction = function (data) {
        var result = rxjs_index.of(true);
        if (this.preAction != undefined) {
            try {
                var res = this.preAction(data);
                if (res instanceof rxjs_index.Observable) {
                    result = res;
                }
            }
            catch (err) {
                result = rxjs_index.throwError(err);
            }
        }
        return result;
    };
    Transition.prototype.executeAction = function (data, options) {
        if (this.action != undefined) {
            try {
                this.action(data, options);
            }
            catch (err) {
                // do nothing
            }
        }
    };
    return Transition;
}());
var StateMachine = /** @class */ (function () {
    function StateMachine(startState) {
        this.startState = startState;
        this.transitions = new Map();
        this.currentState = startState;
    }
    StateMachine.prototype.addTransition = function (fromState, input, transition) {
        if (!this.transitions.has(fromState)) {
            this.transitions.set(fromState, new Map());
        }
        var inputMap = this.transitions.get(fromState);
        if (inputMap != undefined) {
            inputMap.set(input, transition);
        }
    };
    StateMachine.prototype.doTransition = function (input, data, options) {
        var _this = this;
        if (this.transitionInProgress != undefined) {
            return this.transitionInProgress;
        }
        var inputMap = this.transitions.get(this.currentState);
        if (inputMap != undefined) {
            var transition_1 = inputMap.get(input);
            if (transition_1 != undefined) {
                var observable = transition_1.executePreAction(data)
                    .pipe(rxjs_operators.take(1), rxjs_operators.mergeMap(function (val) {
                    if (val) {
                        return rxjs_index.range(1, 2);
                    }
                    else {
                        return rxjs_index.throwError('transition was aborted');
                    }
                }), rxjs_operators.map(function (val) {
                    if (val === 1) {
                        // indicate to the subscriber that the preAction was successfully executed,
                        // but the actual action has not yet been executed
                        return rxjs_index.of(_this.currentState);
                    }
                    var nextState = transition_1.getNextState();
                    if (nextState != undefined) {
                        _this.currentState = nextState;
                    }
                    transition_1.executeAction(data, options);
                    _this.transitionInProgress = undefined;
                    return rxjs_index.EMPTY;
                }), rxjs_operators.mergeAll(), rxjs_operators.catchError(function (err) {
                    _this.transitionInProgress = undefined;
                    return rxjs_index.throwError(err);
                }));
                this.transitionInProgress = observable;
                return this.transitionInProgress;
            }
        }
        return rxjs_index.EMPTY;
    };
    return StateMachine;
}());
var AuthStateMachineService = /** @class */ (function () {
    function AuthStateMachineService(eventBusService, bufferService) {
        var _this = this;
        this.eventBusService = eventBusService;
        this.bufferService = bufferService;
        this.stateMachine = new StateMachine(exports.AUTH_STATES.UNINITIALIZED);
        var doLogout = new Transition(exports.AUTH_STATES.LOGGED_OUT, function (data) { return _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.LOGGED_OUT, data); }, function (data) { return _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.PRE_LOGGED_OUT, data); });
        var doLogin = new Transition(exports.AUTH_STATES.LOGGED_IN, function (data) { return _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.LOGGED_IN, data); }, function (data) { return _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.PRE_LOGGED_IN, data); });
        var rejectUnauthorizedRequest = new Transition(undefined, function (data, options) {
            if (options != undefined && options.subject != undefined) {
                options.subject.error(undefined);
            }
        });
        this.stateMachine.addTransition(exports.AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.LOGOUT, doLogout);
        this.stateMachine.addTransition(exports.AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.LOGIN, doLogin);
        this.stateMachine.addTransition(exports.AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.UNAUTHORIZED, rejectUnauthorizedRequest);
        this.stateMachine.addTransition(exports.AUTH_STATES.LOGGED_OUT, AUTH_INPUTS.LOGIN, doLogin);
        this.stateMachine.addTransition(exports.AUTH_STATES.LOGGED_OUT, AUTH_INPUTS.UNAUTHORIZED, rejectUnauthorizedRequest);
        this.stateMachine.addTransition(exports.AUTH_STATES.LOGGED_IN, AUTH_INPUTS.LOGOUT, doLogout);
        this.stateMachine.addTransition(exports.AUTH_STATES.LOGGED_IN, AUTH_INPUTS.UNAUTHORIZED, new Transition(exports.AUTH_STATES.LOGIN_REQUIRED, function (data, options) {
            if (options != undefined && options.subject != undefined) {
                _this.bufferService.addRetrySubject(options.subject);
            }
            _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.UNAUTHORIZED, data);
        }, function (data) { return _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.PRE_UNAUTHORIZED, data); }));
        this.stateMachine.addTransition(exports.AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.LOGIN, new Transition(exports.AUTH_STATES.LOGGED_IN, function (data, options) {
            _this.bufferService.triggerRetry(options && options.updater);
            _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.LOGIN_CONFIRMED, data);
        }, function (data) { return _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.PRE_LOGIN_CONFIRMED, data); }));
        this.stateMachine.addTransition(exports.AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.LOGOUT, new Transition(exports.AUTH_STATES.LOGGED_OUT, function (data, options) {
            _this.bufferService.rejectAll(options && options.reason);
            _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.LOGIN_REJECTED, data);
            _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.LOGGED_OUT, data);
        }, function (data) {
            _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.PRE_LOGIN_REJECTED, data);
            _this.eventBusService.sendAuthEvent(exports.AUTH_EVENTS.PRE_LOGGED_OUT, data);
        }));
        this.stateMachine.addTransition(exports.AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.UNAUTHORIZED, new Transition(undefined, function (data, options) {
            if (options != undefined && options.subject != undefined) {
                _this.bufferService.addRetrySubject(options.subject);
            }
        }));
    }
    AuthStateMachineService.prototype.doTransition = function (input, data, options) {
        return this.stateMachine.doTransition(input, data, options);
    };
    AuthStateMachineService.prototype.getCurrentState = function () {
        return this.stateMachine.currentState;
    };
    AuthStateMachineService = __decorate$3([
        _angular_core.Injectable(),
        __metadata$2("design:paramtypes", [AuthEventBusService, AuthBufferService])
    ], AuthStateMachineService);
    return AuthStateMachineService;
}());

var __decorate$2 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata$1 = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AuthService = /** @class */ (function () {
    function AuthService(stateMachine, eventBusService) {
        this.stateMachine = stateMachine;
        this.eventBusService = eventBusService;
        this.configUpdaterFunction = function (req) { return req; };
    }
    AuthService.prototype.getConfigUpdaterFunction = function () {
        return this.configUpdaterFunction;
    };
    AuthService.prototype.setConfigUpdaterFunction = function (updater) {
        this.configUpdaterFunction = updater;
    };
    AuthService.prototype.getEventBus = function () {
        return this.eventBusService.eventBus;
    };
    AuthService.prototype.getCurrentState = function () {
        return this.stateMachine.getCurrentState();
    };
    AuthService.prototype.login = function (updater, data) {
        return this.stateMachine.doTransition(AUTH_INPUTS.LOGIN, data, { updater: updater });
    };
    AuthService.prototype.logout = function (reason, data) {
        return this.stateMachine.doTransition(AUTH_INPUTS.LOGOUT, data, { reason: reason });
    };
    AuthService = __decorate$2([
        _angular_core.Injectable(),
        __metadata$1("design:paramtypes", [AuthStateMachineService, AuthEventBusService])
    ], AuthService);
    return AuthService;
}());

var __decorate$1 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AuthInterceptorService = /** @class */ (function () {
    function AuthInterceptorService(authService, stateMachine) {
        this.authService = authService;
        this.stateMachine = stateMachine;
    }
    AuthInterceptorService.prototype.intercept = function (req, next) {
        var _this = this;
        var requestActual = req;
        if (req.headers.has(IGNORE_AUTH_HEADER_NAME)) {
            if (req.headers.get(IGNORE_AUTH_HEADER_NAME)) {
                console.warn('Your request has a value for the IGNORE_AUTH_HEADER_NAME. It will be deleted: ', req);
            }
            requestActual = req.clone({ headers: req.headers.delete(IGNORE_AUTH_HEADER_NAME) });
            return next.handle(requestActual);
        }
        else {
            requestActual = this.updateHttpRequest(req);
            return next
                .handle(requestActual)
                .pipe(rxjs_operators.catchError(function (error) {
                if (error instanceof _angular_common_http.HttpErrorResponse && error.status === 401) {
                    var subject = new rxjs_index.Subject();
                    _this.stateMachine.doTransition(AUTH_INPUTS.UNAUTHORIZED, error, { subject: subject }).subscribe();
                    return subject
                        .pipe(rxjs_operators.take(1), rxjs_operators.catchError(function (err) { return rxjs_index.throwError(err || error); }), rxjs_operators.mergeMap(function (updater) {
                        var updatedRequest = requestActual;
                        if (updater != undefined) {
                            updatedRequest = updater(updatedRequest);
                        }
                        else {
                            updatedRequest = _this.updateHttpRequest(updatedRequest);
                        }
                        return next.handle(updatedRequest);
                    }));
                }
                return rxjs_index.throwError(error);
            }));
        }
    };
    AuthInterceptorService.prototype.updateHttpRequest = function (request) {
        return this.authService.getConfigUpdaterFunction()(request);
    };
    AuthInterceptorService = __decorate$1([
        _angular_core.Injectable(),
        __metadata("design:paramtypes", [AuthService, AuthStateMachineService])
    ], AuthInterceptorService);
    return AuthInterceptorService;
}());

var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AngularAuthStatesModule = /** @class */ (function () {
    function AngularAuthStatesModule() {
    }
    AngularAuthStatesModule = __decorate([
        _angular_core.NgModule({
            imports: [
                _angular_common.CommonModule
            ],
            providers: [
                {
                    provide: _angular_common_http.HTTP_INTERCEPTORS,
                    useClass: AuthInterceptorService,
                    multi: true,
                },
                AuthService,
                AuthBufferService,
                AuthStateMachineService,
                AuthEventBusService
            ]
        })
    ], AngularAuthStatesModule);
    return AngularAuthStatesModule;
}());

exports.AngularAuthStatesModule = AngularAuthStatesModule;
exports.AuthService = AuthService;
exports.IGNORE_AUTH_HEADER_NAME = IGNORE_AUTH_HEADER_NAME;
exports.AuthEvent = AuthEvent;

Object.defineProperty(exports, '__esModule', { value: true });

})));
