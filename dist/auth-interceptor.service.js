var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject, throwError } from 'rxjs/index';
import { mergeMap, catchError, take } from 'rxjs/operators';
import { IGNORE_AUTH_HEADER_NAME, AUTH_INPUTS } from "./constants";
import { AuthService } from "./auth.service";
import { AuthStateMachineService } from "./auth-state-machine.service";
var AuthInterceptorService = /** @class */ (function () {
    function AuthInterceptorService(authService, stateMachine) {
        this.authService = authService;
        this.stateMachine = stateMachine;
    }
    AuthInterceptorService.prototype.intercept = function (req, next) {
        var _this = this;
        var requestActual = req;
        if (req.headers.has(IGNORE_AUTH_HEADER_NAME)) {
            if (req.headers.get(IGNORE_AUTH_HEADER_NAME)) {
                console.warn('Your request has a value for the IGNORE_AUTH_HEADER_NAME. It will be deleted: ', req);
            }
            requestActual = req.clone({ headers: req.headers.delete(IGNORE_AUTH_HEADER_NAME) });
            return next.handle(requestActual);
        }
        else {
            requestActual = this.updateHttpRequest(req);
            return next
                .handle(requestActual)
                .pipe(catchError(function (error) {
                if (error instanceof HttpErrorResponse && error.status === 401) {
                    var subject = new Subject();
                    _this.stateMachine.doTransition(AUTH_INPUTS.UNAUTHORIZED, error, { subject: subject }).subscribe();
                    return subject
                        .pipe(take(1), catchError(function (err) { return throwError(err || error); }), mergeMap(function (updater) {
                        var updatedRequest = requestActual;
                        if (updater != undefined) {
                            updatedRequest = updater(updatedRequest);
                        }
                        else {
                            updatedRequest = _this.updateHttpRequest(updatedRequest);
                        }
                        return next.handle(updatedRequest);
                    }));
                }
                return throwError(error);
            }));
        }
    };
    AuthInterceptorService.prototype.updateHttpRequest = function (request) {
        return this.authService.getConfigUpdaterFunction()(request);
    };
    AuthInterceptorService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [AuthService, AuthStateMachineService])
    ], AuthInterceptorService);
    return AuthInterceptorService;
}());
export { AuthInterceptorService };
//# sourceMappingURL=auth-interceptor.service.js.map