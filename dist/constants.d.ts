export declare const IGNORE_AUTH_HEADER_NAME = "ng-AuthService-ignoreAuthModule";
export declare enum AUTH_STATES {
    UNINITIALIZED = 0,
    LOGGED_OUT = 1,
    LOGGED_IN = 2,
    LOGIN_REQUIRED = 3
}
export declare enum AUTH_INPUTS {
    LOGOUT = 0,
    LOGIN = 1,
    UNAUTHORIZED = 2
}
export declare enum AUTH_EVENTS {
    PRE_LOGGED_OUT = 0,
    LOGGED_OUT = 1,
    PRE_LOGGED_IN = 2,
    LOGGED_IN = 3,
    PRE_UNAUTHORIZED = 4,
    UNAUTHORIZED = 5,
    PRE_LOGIN_CONFIRMED = 6,
    LOGIN_CONFIRMED = 7,
    PRE_LOGIN_REJECTED = 8,
    LOGIN_REJECTED = 9
}
