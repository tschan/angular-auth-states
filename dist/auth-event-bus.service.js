var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/index';
var AuthEvent = /** @class */ (function () {
    function AuthEvent(eventType, data) {
        this.eventType = eventType;
        this.data = data;
    }
    return AuthEvent;
}());
export { AuthEvent };
var AuthEventBusService = /** @class */ (function () {
    function AuthEventBusService() {
        this.eventBus = new Subject();
    }
    AuthEventBusService.prototype.sendAuthEvent = function (eventType, data) {
        this.eventBus.next(new AuthEvent(eventType, data));
    };
    AuthEventBusService = __decorate([
        Injectable()
    ], AuthEventBusService);
    return AuthEventBusService;
}());
export { AuthEventBusService };
//# sourceMappingURL=auth-event-bus.service.js.map