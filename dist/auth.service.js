var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { AUTH_INPUTS } from "./constants";
import { AuthStateMachineService } from "./auth-state-machine.service";
import { AuthEventBusService } from "./auth-event-bus.service";
var AuthService = /** @class */ (function () {
    function AuthService(stateMachine, eventBusService) {
        this.stateMachine = stateMachine;
        this.eventBusService = eventBusService;
        this.configUpdaterFunction = function (req) { return req; };
    }
    AuthService.prototype.getConfigUpdaterFunction = function () {
        return this.configUpdaterFunction;
    };
    AuthService.prototype.setConfigUpdaterFunction = function (updater) {
        this.configUpdaterFunction = updater;
    };
    AuthService.prototype.getEventBus = function () {
        return this.eventBusService.eventBus;
    };
    AuthService.prototype.getCurrentState = function () {
        return this.stateMachine.getCurrentState();
    };
    AuthService.prototype.login = function (updater, data) {
        return this.stateMachine.doTransition(AUTH_INPUTS.LOGIN, data, { updater: updater });
    };
    AuthService.prototype.logout = function (reason, data) {
        return this.stateMachine.doTransition(AUTH_INPUTS.LOGOUT, data, { reason: reason });
    };
    AuthService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [AuthStateMachineService, AuthEventBusService])
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map