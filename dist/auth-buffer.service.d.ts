import { Subject } from 'rxjs/index';
import { RequestUpdater } from "./types";
export declare class AuthBufferService {
    private retrySubjects;
    addRetrySubject(subject: Subject<RequestUpdater>): void;
    triggerRetry(updater?: RequestUpdater): void;
    rejectAll(reason?: any): void;
}
