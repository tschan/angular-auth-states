import { HttpRequest } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
export declare type RequestUpdater = (req: HttpRequest<any>) => HttpRequest<any>;
export declare type TransitionOptions = {
    subject?: Subject<RequestUpdater>;
    updater?: RequestUpdater;
    reason?: any;
};
