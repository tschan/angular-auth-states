import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { AuthService } from "./auth.service";
import { AuthStateMachineService } from "./auth-state-machine.service";
export declare class AuthInterceptorService implements HttpInterceptor {
    private authService;
    private stateMachine;
    constructor(authService: AuthService, stateMachine: AuthStateMachineService);
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
    private updateHttpRequest;
}
