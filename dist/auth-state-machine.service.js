var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Observable, EMPTY, throwError, range, of } from 'rxjs/index';
import { take, map, mergeMap, mergeAll, catchError } from 'rxjs/operators';
import { AUTH_STATES, AUTH_INPUTS, AUTH_EVENTS } from "./constants";
import { AuthEventBusService } from "./auth-event-bus.service";
import { AuthBufferService } from "./auth-buffer.service";
var Transition = /** @class */ (function () {
    function Transition(nextState, action, preAction) {
        this.nextState = nextState;
        this.action = action;
        this.preAction = preAction;
    }
    Transition.prototype.getNextState = function () {
        return this.nextState;
    };
    Transition.prototype.executePreAction = function (data) {
        var result = of(true);
        if (this.preAction != undefined) {
            try {
                var res = this.preAction(data);
                if (res instanceof Observable) {
                    result = res;
                }
            }
            catch (err) {
                result = throwError(err);
            }
        }
        return result;
    };
    Transition.prototype.executeAction = function (data, options) {
        if (this.action != undefined) {
            try {
                this.action(data, options);
            }
            catch (err) {
                // do nothing
            }
        }
    };
    return Transition;
}());
var StateMachine = /** @class */ (function () {
    function StateMachine(startState) {
        this.startState = startState;
        this.transitions = new Map();
        this.currentState = startState;
    }
    StateMachine.prototype.addTransition = function (fromState, input, transition) {
        if (!this.transitions.has(fromState)) {
            this.transitions.set(fromState, new Map());
        }
        var inputMap = this.transitions.get(fromState);
        if (inputMap != undefined) {
            inputMap.set(input, transition);
        }
    };
    StateMachine.prototype.doTransition = function (input, data, options) {
        var _this = this;
        if (this.transitionInProgress != undefined) {
            return this.transitionInProgress;
        }
        var inputMap = this.transitions.get(this.currentState);
        if (inputMap != undefined) {
            var transition_1 = inputMap.get(input);
            if (transition_1 != undefined) {
                var observable = transition_1.executePreAction(data)
                    .pipe(take(1), mergeMap(function (val) {
                    if (val) {
                        return range(1, 2);
                    }
                    else {
                        return throwError('transition was aborted');
                    }
                }), map(function (val) {
                    if (val === 1) {
                        // indicate to the subscriber that the preAction was successfully executed,
                        // but the actual action has not yet been executed
                        return of(_this.currentState);
                    }
                    var nextState = transition_1.getNextState();
                    if (nextState != undefined) {
                        _this.currentState = nextState;
                    }
                    transition_1.executeAction(data, options);
                    _this.transitionInProgress = undefined;
                    return EMPTY;
                }), mergeAll(), catchError(function (err) {
                    _this.transitionInProgress = undefined;
                    return throwError(err);
                }));
                this.transitionInProgress = observable;
                return this.transitionInProgress;
            }
        }
        return EMPTY;
    };
    return StateMachine;
}());
var AuthStateMachineService = /** @class */ (function () {
    function AuthStateMachineService(eventBusService, bufferService) {
        var _this = this;
        this.eventBusService = eventBusService;
        this.bufferService = bufferService;
        this.stateMachine = new StateMachine(AUTH_STATES.UNINITIALIZED);
        var doLogout = new Transition(AUTH_STATES.LOGGED_OUT, function (data) { return _this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGGED_OUT, data); }, function (data) { return _this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGGED_OUT, data); });
        var doLogin = new Transition(AUTH_STATES.LOGGED_IN, function (data) { return _this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGGED_IN, data); }, function (data) { return _this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGGED_IN, data); });
        var rejectUnauthorizedRequest = new Transition(undefined, function (data, options) {
            if (options != undefined && options.subject != undefined) {
                options.subject.error(undefined);
            }
        });
        this.stateMachine.addTransition(AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.LOGOUT, doLogout);
        this.stateMachine.addTransition(AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.LOGIN, doLogin);
        this.stateMachine.addTransition(AUTH_STATES.UNINITIALIZED, AUTH_INPUTS.UNAUTHORIZED, rejectUnauthorizedRequest);
        this.stateMachine.addTransition(AUTH_STATES.LOGGED_OUT, AUTH_INPUTS.LOGIN, doLogin);
        this.stateMachine.addTransition(AUTH_STATES.LOGGED_OUT, AUTH_INPUTS.UNAUTHORIZED, rejectUnauthorizedRequest);
        this.stateMachine.addTransition(AUTH_STATES.LOGGED_IN, AUTH_INPUTS.LOGOUT, doLogout);
        this.stateMachine.addTransition(AUTH_STATES.LOGGED_IN, AUTH_INPUTS.UNAUTHORIZED, new Transition(AUTH_STATES.LOGIN_REQUIRED, function (data, options) {
            if (options != undefined && options.subject != undefined) {
                _this.bufferService.addRetrySubject(options.subject);
            }
            _this.eventBusService.sendAuthEvent(AUTH_EVENTS.UNAUTHORIZED, data);
        }, function (data) { return _this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_UNAUTHORIZED, data); }));
        this.stateMachine.addTransition(AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.LOGIN, new Transition(AUTH_STATES.LOGGED_IN, function (data, options) {
            _this.bufferService.triggerRetry(options && options.updater);
            _this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGIN_CONFIRMED, data);
        }, function (data) { return _this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGIN_CONFIRMED, data); }));
        this.stateMachine.addTransition(AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.LOGOUT, new Transition(AUTH_STATES.LOGGED_OUT, function (data, options) {
            _this.bufferService.rejectAll(options && options.reason);
            _this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGIN_REJECTED, data);
            _this.eventBusService.sendAuthEvent(AUTH_EVENTS.LOGGED_OUT, data);
        }, function (data) {
            _this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGIN_REJECTED, data);
            _this.eventBusService.sendAuthEvent(AUTH_EVENTS.PRE_LOGGED_OUT, data);
        }));
        this.stateMachine.addTransition(AUTH_STATES.LOGIN_REQUIRED, AUTH_INPUTS.UNAUTHORIZED, new Transition(undefined, function (data, options) {
            if (options != undefined && options.subject != undefined) {
                _this.bufferService.addRetrySubject(options.subject);
            }
        }));
    }
    AuthStateMachineService.prototype.doTransition = function (input, data, options) {
        return this.stateMachine.doTransition(input, data, options);
    };
    AuthStateMachineService.prototype.getCurrentState = function () {
        return this.stateMachine.currentState;
    };
    AuthStateMachineService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [AuthEventBusService, AuthBufferService])
    ], AuthStateMachineService);
    return AuthStateMachineService;
}());
export { AuthStateMachineService };
//# sourceMappingURL=auth-state-machine.service.js.map