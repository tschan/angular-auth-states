import { AUTH_STATES } from "./constants";
import { AuthStateMachineService } from "./auth-state-machine.service";
import { AuthEventBusService, AuthEvent } from "./auth-event-bus.service";
import { RequestUpdater } from "./types";
export declare class AuthService {
    private stateMachine;
    private eventBusService;
    private configUpdaterFunction;
    constructor(stateMachine: AuthStateMachineService, eventBusService: AuthEventBusService);
    getConfigUpdaterFunction(): RequestUpdater;
    setConfigUpdaterFunction(updater: RequestUpdater): void;
    getEventBus(): import("rxjs/internal/Subject").Subject<AuthEvent>;
    getCurrentState(): AUTH_STATES;
    login(updater?: RequestUpdater, data?: any): import("rxjs/internal/Observable").Observable<AUTH_STATES>;
    logout(reason?: any, data?: any): import("rxjs/internal/Observable").Observable<AUTH_STATES>;
}
