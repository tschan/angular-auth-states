/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
import * as i0 from "@angular/core";
import * as i1 from "./angular-auth-states.module";
import * as i2 from "@angular/common";
import * as i3 from "./auth-event-bus.service";
import * as i4 from "./auth-buffer.service";
import * as i5 from "./auth-state-machine.service";
import * as i6 from "./auth.service";
import * as i7 from "@angular/common/http";
import * as i8 from "./auth-interceptor.service";
var AngularAuthStatesModuleNgFactory = i0.ɵcmf(i1.AngularAuthStatesModule, [], function (_l) { return i0.ɵmod([i0.ɵmpd(512, i0.ComponentFactoryResolver, i0.ɵCodegenComponentFactoryResolver, [[8, []], [3, i0.ComponentFactoryResolver], i0.NgModuleRef]), i0.ɵmpd(4608, i2.NgLocalization, i2.NgLocaleLocalization, [i0.LOCALE_ID, [2, i2.ɵangular_packages_common_common_a]]), i0.ɵmpd(4608, i3.AuthEventBusService, i3.AuthEventBusService, []), i0.ɵmpd(4608, i4.AuthBufferService, i4.AuthBufferService, []), i0.ɵmpd(4608, i5.AuthStateMachineService, i5.AuthStateMachineService, [i3.AuthEventBusService, i4.AuthBufferService]), i0.ɵmpd(4608, i6.AuthService, i6.AuthService, [i5.AuthStateMachineService, i3.AuthEventBusService]), i0.ɵmpd(5120, i7.HTTP_INTERCEPTORS, function (p0_0, p0_1) { return [new i8.AuthInterceptorService(p0_0, p0_1)]; }, [i6.AuthService, i5.AuthStateMachineService]), i0.ɵmpd(1073742336, i2.CommonModule, i2.CommonModule, []), i0.ɵmpd(1073742336, i1.AngularAuthStatesModule, i1.AngularAuthStatesModule, [])]); });
export { AngularAuthStatesModuleNgFactory as AngularAuthStatesModuleNgFactory };
//# sourceMappingURL=angular-auth-states.module.ngfactory.js.map