import { Subject } from 'rxjs/index';
import { AUTH_EVENTS } from "./constants";
export declare class AuthEvent {
    eventType: AUTH_EVENTS;
    data?: any;
    constructor(eventType: AUTH_EVENTS, data?: any);
}
export declare class AuthEventBusService {
    readonly eventBus: Subject<AuthEvent>;
    sendAuthEvent(eventType: AUTH_EVENTS, data?: any): void;
}
