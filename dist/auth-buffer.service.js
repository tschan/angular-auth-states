var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
var AuthBufferService = /** @class */ (function () {
    function AuthBufferService() {
        this.retrySubjects = [];
    }
    AuthBufferService.prototype.addRetrySubject = function (subject) {
        this.retrySubjects.push(subject);
    };
    AuthBufferService.prototype.triggerRetry = function (updater) {
        this.retrySubjects.forEach(function (subject) {
            subject.next(updater);
            subject.complete();
        });
        this.retrySubjects = [];
    };
    AuthBufferService.prototype.rejectAll = function (reason) {
        this.retrySubjects.forEach(function (subject) {
            subject.error(reason);
        });
        this.retrySubjects = [];
    };
    AuthBufferService = __decorate([
        Injectable()
    ], AuthBufferService);
    return AuthBufferService;
}());
export { AuthBufferService };
//# sourceMappingURL=auth-buffer.service.js.map